{
	nixpkgs ? import <nixpkgs> {},

	lib ? nixpkgs.lib,
	pkgs ? nixpkgs.pkgs,

	commit ? builtins.getEnv "CI_COMMIT_SHA",
	version ? if commit == "" then "dev" else lib.substring 0 8 commit,

	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nmattia/naersk/archive/master.tar.gz") {},
}:

{
	journal-forwarder = pkgs.resholve.mkDerivation {
		pname = "journal-forwarder";
		version = version;
		
		meta = {
			description = "Forward journald logs over HTTP somewhat reliably.";
			homepage = https://github.com/kevincox/journal-forwarder;
		};
		
		src = builtins.filterSource (name: type:
			(lib.hasPrefix (toString ./journal-forwarder.sh) name)
		) ./.;

		solutions.default = {
			scripts = [ "bin/journal-forwarder" ];
			interpreter = "${pkgs.bash}/bin/bash";
			inputs = with pkgs; [
				coreutils
				curl
				gnused
				jq
				systemd
				util-linux
			];
		};
		
		installPhase = ''
			install -Dm755 journal-forwarder.sh "$out/bin/journal-forwarder"
		'';
	};

	rust = naersk.buildPackage {
		root = pkgs.nix-gitignore.gitignoreSource [
			"*.md"
			"*.nix"
			"*.sh"
			".*"
		] ./.;
		buildInputs = with pkgs; [
			openssl
			pkg-config
			systemd
		];
	};
}
