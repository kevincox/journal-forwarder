use std::io::Write;

fn env(name: &str) -> Option<String> {
	match std::env::var(name) {
		Ok(v) => Some(v),
		Err(std::env::VarError::NotPresent) => None,
		Err(err) => panic!("Could not read {}: {}", name, err),
	}
}

struct Config {
	batch: u32,
	cursor_path: std::path::PathBuf,
	debug: bool,
	method: reqwest::Method,
	url: url::Url,
}

impl Config {
	fn from_env() -> Self {
		if env("JF_FILTER").is_some() {
			panic!("JF_FILTER is not supported in the Rust version.")
		}
		Config {
			debug: env("JF_DEBUG")
				.map(|v| !v.is_empty())
				.unwrap_or(false),
			batch: env("JF_BATCH")
				.map(|n| n.parse().expect("JF_BATCH is not an integer"))
				.unwrap_or(100),
			cursor_path: env("JF_CURSOR")
				.unwrap_or_else(|| "journal-forwarder.cursor".into())
				.into(),
			method: env("JF_METHOD")
				.map(|m| m.parse().expect("JF_METHOD is invalid"))
				.unwrap_or(reqwest::Method::POST),
			url: env("JF_URL")
				.or_else(|| env("JF_URL_SRC")
					.map(|path| std::fs::read_to_string(path).expect("Could not read JF_URL_SRC")))
				.expect("JF_URL or JF_URL_SRC env variables are required.")
				.parse().expect("JF_URL is not a valid URL"),
		}
	}
}

fn log(
	message_id: impl Into<String>,
	message: impl ToString,
	priority: u8,
	fields: impl IntoIterator<Item=(String, String)>,
) {
	let mut e = journald::JournalEntry::new();
	e.fields.insert("MESSAGE_ID".into(), message_id.into());
	e.fields.insert("MESSAGE".into(), message.to_string());
	e.fields.insert("PRIORITY".into(), priority.to_string());
	e.fields.insert("SYSLOG_IDENTIFIER".into(), "journal-forwarder".into());
	e.fields.extend(fields);
	if let Err(err) = journald::writer::submit(&e) {
		eprintln!("{:?}", e);
		eprintln!("Failed to write to the journal: {}", err);
	}
}

fn main() {
	let config = Config::from_env();

	let mut journal = journald::reader::JournalReader::open(&Default::default()).unwrap();
	let mut cursor = String::new();

	match std::fs::read_to_string(&config.cursor_path) {
		Ok(c) => {
			cursor.clone_from(&c);
			if let Err(err) = journal.seek(journald::reader::JournalSeek::Cursor(c)) {
				log(
					"75a7247ca3324431b039a3d66ca39543",
					"Invalid cursor. Falling back to current time, messages may be missed.",
					3,
					[
						("INVALID_CURSOR".to_string(), cursor.clone()),
						("JOURNALD_RETURNED".into(), err.to_string()),
					]);
				journal.seek(journald::reader::JournalSeek::Tail).unwrap();
			}
		},
		Err(err) => match err.kind() {
			std::io::ErrorKind::NotFound => {
				log(
					"f2e153ec33cc4037a9ca1a4180a598de",
					"This appears to be the first run, running from start.",
					5,
					[]);
				journal.seek(journald::reader::JournalSeek::Head).unwrap();
			}
			err => panic!("Error reading saved cursor: {}", err)
		}
	}

	let client = reqwest::blocking::Client::builder()
		.user_agent("journal-forwarder.kevincox.ca/1")
		.connect_timeout(std::time::Duration::from_secs(60))
		.timeout(std::time::Duration::from_secs(10 * 60))
		.build().unwrap();

	log(
		"4f56c3e133bc411383d7200165e2e866",
		format!("Starting with cursor {:?}", cursor),
		6,
		[("START_CURSOR".to_string(), cursor.clone())]);

	let mut batch = Vec::new();

	loop {
		for _ in 0..config.batch {
			let entry = journal.next_entry()
				.expect("Could not read entry from journal.");
			let entry = match entry {
				Some(e) => e,
				None => break,
			};

			serde_json::to_writer(&mut batch, &entry.fields).unwrap();
			batch.push(b'\n');
			cursor.clear();
			cursor.push_str(entry.get_field("__CURSOR").unwrap());
		}

		if batch.is_empty() {
			journal.wait().expect("Failed to wait for new entries.");
			continue
		}

		if config.debug {
			println!(" --- BATCH BOUNDARY ---");
			std::io::stdout().write_all(&batch).unwrap();
		} else {
			client.request(config.method.clone(), config.url.clone())
				.header("content-type", "application/json")
				.body(batch.clone())
				.send()
				.expect("Failed to send logs.");
		}

		write_atomic::write_file(&config.cursor_path, cursor.as_bytes())
			.expect("failed to checkpoint cursor");

		batch.clear();
	}
}
